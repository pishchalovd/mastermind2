package com.team09.mastermindclient;

import java.io.IOException;

/**
 *  Adds general logic to the business class.
 * @author Danieil Skrinikov
 */
public class MMClientBusiness {
    private MMClientPacket packet;
    
    public MMClientBusiness(){
        super();
        
    }
    
    /**
     * Connects to a server using the specified ip address and port number.
     * @param ip ip address of the server.
     * @param port port number on which the server will be listening.
     * @return true if the connection passed, false if could not connect.
     */
    public boolean createSession(String ip, int port){
        boolean isConnected;
        try{
            packet = new MMClientPacket(ip,port);
            packet.createConncetion();
            isConnected = true;
        }
        catch(IOException ioe){
            isConnected = false;
        }        
        return isConnected;
    }
    
    /**
     * Starts a new game with the server.
     * 
     * @return true if the game has begun, false if it did not.
     */
    public boolean startNewGame(){
        boolean didStart = false;
        
        try{
            byte[] bytes = new byte[5];
            for(int i = 0 ; i < 5 ; i++)
                bytes[i] = 0;
            bytes = packet.sendMessage(bytes);
            
            System.out.println(bytes[0]);
            
            if(bytes[0] == 0)
                didStart = true;
        }
        catch(IOException ioe){
            didStart = false;
        }        
        return didStart;
    }
    
    /**
     * Sends the attempt to the server in order to check if it is true. 
     * Takes a byte array of length 4 and sends it to the server. 
     * Returns an array of length 5 where the first value has information on the 
     * state of the game.
     * 
     * @param attempt array of 4 bytes representing the user's attempt.
     * @return array of 5 bytes representing the server's answer.
     * @throws java.io.IOException if cannot transfer packets.
     */
    public byte[] makeAnAttempt(byte[] attempt) throws IOException{
        byte[] answer = new byte[5];
        answer[0] = 1;
        for(int i = 1 ; i < 5; i++)
            answer[i] = attempt[i-1];
        
        answer = packet.sendMessage(answer);
        
        return answer;      
    }
    
    /**
     * Sends a message to the server stating that the player has surrendered this 
     * round and that he would like to get the correct array of values.
     * 
     * @return array of 5 bytes representing the server's answer.
     * @throws IOException if cannot transfer packets.
     */
    public byte[] surrender() throws IOException{
        byte[] answer = new byte[5];
        for(int i = 0 ; i < 5 ; i++)
            answer[i] = 2;
        
        answer = packet.sendMessage(answer);
        
        return answer;
    }
    
    /**
     * Closes the connection to the server.
     * 
     * @throws IOException if cannot close.
     */
    public void close() throws IOException{
       packet.close();
    }
    
    /**
     * Checks if the socket is closed.
     * 
     * @return true if the session is closed, false if it is open.
     */
    public boolean isClosed(){
        return packet.isClosed();
    }
}
