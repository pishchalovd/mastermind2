package com.team09.mastermindclient;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class FXMLController implements Initializable {

    //Color Constants used for comparision.
    private final Paint COLOR1 = Color.rgb(7, 19, 248, 1);
    private final Paint COLOR2 = Color.rgb(159, 78, 235, 1);
    private final Paint COLOR3 = Color.rgb(5, 255, 5, 1);
    private final Paint COLOR4 = Color.rgb(0, 255, 229, 1);
    private final Paint COLOR5 = Color.rgb(255, 136, 0, 1);
    private final Paint COLOR6 = Color.rgb(255, 0, 178, 1);
    private final Paint COLOR7 = Color.rgb(255, 0, 0, 1);
    private final Paint COLOR8 = Color.rgb(235, 255, 0, 1);
    
    //Instance fields.
    private Paint activeColor;
    private int turn = 10;
    private String ip;
    private int port;
    private MMClientBusiness logic;
    private Paint[] circles;
    
    //Injected fields.
    @FXML
    private AnchorPane AnchorPane;
    @FXML
    private GridPane main_grid;
    @FXML
    private Button btn_attempt;
    @FXML
    private GridPane gridServ;
    @FXML
    private TextField lbl_ip;
    @FXML
    private TextField lbl_port;
    @FXML
    private Label lbl_msg;
    
    //Called right after the UI is set. Using it to display changes.
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        activeColor = Color.rgb(7, 19, 248, 1);
        //gridServ.setVisible(true);
        circles = new Paint[4];
    }
    
    //Creates a new game with an existing connection. If game did not start
    //Opens the server pane dialog.
    @FXML
    private void onNewGameClick(ActionEvent e) {
        boolean didStart = false;
        if (logic != null && !logic.isClosed()) {
            didStart = logic.startNewGame();
        }

        if (!didStart) {
            showServerSelection(null);
            lbl_msg.setText("Could not start new game.");
            
        }
    }
    
    //Called when the user clicks on the main grid.
    //Handles the choice of the user.
    @FXML
    private void onGridClick(MouseEvent e) {
        GridPane source = (GridPane) e.getSource();

        //Looks for the node which was clicked. 
        for (Node n : source.getChildren()) {
            if (n instanceof Circle) {
                if (n.getBoundsInParent().contains(e.getX(), e.getY()) && GridPane.getRowIndex(n) != null && turn == GridPane.getRowIndex(n)) {
                    //Sets the color of the circle to the active color.
                    //And makes it visible.
                    Circle c = (Circle) n;
                    c.setVisible(true);
                    c.setFill(activeColor);

                    //Sets the internal color array to the color. 
                    //Uses GridPane returns null if the column is 0.
                    if (GridPane.getColumnIndex(n) == null) {
                        circles[0] = activeColor;
                    } else {
                        circles[GridPane.getColumnIndex(n)] = activeColor;
                    }
                }
            }
        }
    }

    //Changes the selected color.
    @FXML
    private void onColorChoice(MouseEvent e) {
        GridPane source = (GridPane) e.getSource();

        for (Node n : source.getChildren()) {
            //Checks if the child is a circle and if it is in the specified position
            if (n instanceof Circle && n.getBoundsInParent().contains(e.getX(), e.getY())) {
                
                Circle c = (Circle) n;
                activeColor = c.getFill();
            }
        }
    }

    @FXML
    private void makeAttempt(ActionEvent event) {
        if (circles[0] != null && circles[1] != null && circles[2] != null && circles[3] != null) {
            
            try{
            logic.makeAnAttempt(switchColorsToBytes());            
            turn--;
            circles = new Paint[4];
            }
            catch(IOException ioe){
                
            }
        }
    }

    //Attempts to connect to the server with the supplied information.
    @FXML
    private void connect(ActionEvent event) {
        try {
            //Getting server information
            ip = lbl_ip.getText();
            port = Integer.parseInt(lbl_port.getText());

            //Creating connection
            logic = new MMClientBusiness();
            logic.createSession(ip, port);
            
            onNewGameClick(null);

            //If the connection was created, the server selection pane must be 
            //hidden.
            gridServ.setVisible(false);
        } catch (NumberFormatException e) {
            lbl_msg.setText("One of the supplied parameters is incorrect");
        }
    }

    @FXML
    private void showServerSelection(ActionEvent event) {
        gridServ.setVisible(true);
    }

    @FXML
    private void showAbout(ActionEvent event) {
    }

    @FXML
    private void onGiveUp(ActionEvent event) {
        try {
            if (logic != null && !logic.isClosed()) {
                logic.surrender();
            }
        } catch (IOException ioe) {
            manageReadWriteExceptions();
        }
    }

    @FXML
    private void showHelp(ActionEvent event) {
    }

    private void manageReadWriteExceptions() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    //Takes the color of each cell in the circles array and gives it a numeric value.
    private byte[] switchColorsToBytes(){
        byte[] nums = new byte[circles.length];
        
        for(int i = 0 ; i < circles.length ; i++){
            if(circles[i].equals(COLOR1))
                nums[i] = 7;
            else if(circles[i].equals(COLOR2))
                nums[i] = 6;
            else if(circles[i].equals(COLOR3))
                nums[i] = 5;
            else if(circles[i].equals(COLOR4))
                nums[i] = 4;
            else if(circles[i].equals(COLOR5))
                nums[i] = 3;
            else if(circles[i].equals(COLOR6))
                nums[i] = 2;
            else if(circles[i].equals(COLOR7))
                nums[i] = 1;
            else
                nums[i] = 0;
        }
        return nums;
    }
    
    //Switches color values back to the calling color.
    private void switchBytesToColor(byte[] nums){
        for(int  i = 0 ; i < nums.length ; i++){
            switch (nums[i]) {
                case 0:
                    circles[i] = COLOR8;
                    break;
                case 1:
                    circles[i] = COLOR7;
                    break;
                case 2:
                    circles[i] = COLOR6;
                    break;
                case 3:
                    circles[i] = COLOR5;
                    break;
                case 4:
                    circles[i] = COLOR4;
                    break;
                case 5:
                    circles[i] = COLOR3;
                    break;
                case 6:
                    circles[i] = COLOR2;
                    break;
                default:
                    circles[i] = COLOR1;
                    break;
            }
        }
    }

}
